# Test Driven Development (TDD) Examples

## Test Driven Development Workflow
The TDD workflow includes writing tests into the implementation workflow of a software itself. 
It contains three steps.

1. Write a failing test
2. Write just the code to make the test pass.
3. Refactor the implemented code

On CppCon 2020 Phil Nash gave a talk on [TDD](https://www.youtube.com/watch?v=N2gTxeIHMP0) by which the slides and the 
example is inspired.

## Usecase: 1-D Heat Equation

In this example the partial differential equation for the 1-D [heat equation](https://en.wikipedia.org/wiki/Heat_equation)
with Dirichlet Boundary conditions is solved using an explicit 
[finite difference method](https://en.wikipedia.org/wiki/Finite_difference_method#Explicit_method).
```math
\partial_t u(x,t) = \partial^2_x u(x,t)\\
u(0,t) = u(L_\text{max},t) = 0 
```
The equation can be solved if an initial condition $u(x,0)$ is provided.

The simulation is configured through command line arguments and file containing the initial condition which receives 
the output of the thermal distribution.
### Command Line Input arguments
| Name                | Description                                                                              | Default                   | Datatype |
|---------------------|------------------------------------------------------------------------------------------|---------------------------|----------|
| ` --sim-filename `  | File with initial condition for the heat equation<br/> which is appended by the solution | `thermalDistribution.dat` | `string` |
| ` --sim-time-step`  | Timestep `dt` by which the distribution function<br/> is advanced per propagation step   | `0.05`                    | `double` |
| ` --sim-end-time `  | Final time `T` up to which the simulation is executed                                    | `10`                      | `double` |

### File format with initial condition and result data
The file has a csv format with the first two lines to be ignored.
The first line defines the grid used for the configuration providing the length of the grid starting at zero.
The second line is explanatory to the lines below which contain the actual values of the thermal distribution.
Braces behind each row provide a short explanation.
```asm
L_max = <LengthOfTheDomain>, N = <NumberOfCellsInTheDomain> (Grid Configuration)
t                      , 0                    , dx                         , ... , L_max (Descriptive Line)
0   , <LowerBoundaryValue>, <ThermalDistributionValue>, ... , <UpperBoundaryValue> (Initial Condition)
dt  , <LowerBoundaryValue>, <ThermalDistributionValue>, ... , <UpperBoundaryValue> (Propagation Data)
2*dt, <LowerBoundaryValue>, <ThermalDistributionValue>, ... , <UpperBoundaryValue> (Propagation Data)
3*dt, <LowerBoundaryValue>, <ThermalDistributionValue>, ... , <UpperBoundaryValue> (Propagation Data)
...
t>=T, <LowerBoundaryValue>, <ThermalDistributionValue>, ... , <UpperBoundaryValue> (Propagation Data)
```

## Integration test

To check whether the single units of the simulation work together properly the following integration test can be used.\
```math
u(x,t) = \text{e}^{-t}\sin (x)\\
u(x,0) = \sin(x)\\
x\in[0,\pi]
```
To run the integration test execute the following steps in a console
```sh
cd ./integration_test
cp <PathToSimulationExecutable> ./Simulation
./Simulation --sim-filename thermalDistribution.test.dat
./plot_thermal_distribution.py
```
These commands should return the following plot.
<div style="text-align: center;">

![Integration Test Results](integration_test/HeatEquationResultsIntegrationTest.png )

</div>

## Upcoming
* Examples of TDD implementations of the use case in Python, Julia
* Documentation workflows for the provided source code
* Documentation improvements for the use case and the software design

## Long term goal
Provide a repository containing a simple use case (1-D heat equation) to show good practices for different programming languages.\
Can this be turned into a tutorial for 2-3 days how to set up a new project properly in a given programming language?
