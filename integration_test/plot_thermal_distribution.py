#! /usr/bin/python3
from matplotlib import pyplot as plt
import matplotlib as mpl
import numpy as np
import pandas as pd

Ures = pd.read_csv('./thermalDistribution.test.dat',header=None,skiprows=2)
x = np.arange(0,np.pi+2*np.pi/10,np.pi/10)
X, T = np.meshgrid(x,Ures[Ures.columns[0]])
Ures = Ures.drop(axis='columns',columns=Ures.columns[0])

fig, ax = plt.subplots(1,1)
ax.pcolormesh(X,T,Ures,cmap='inferno')
ax.set_ylim(T.min(),T.max()/5)
ax.set_xlabel("x")
ax.set_ylabel("t")
plt.tight_layout()
plt.show(block=True)
fig.show()


#X, T = np.mgrid[0:np.pi:11j,0:3:10j]
#U = np.exp(-T)*np.sin(X)
#
#fig, ax = plt.subplots(1,1, figsize = (10*cm2inch, 4*cm2inch),dpi=200)
#ax.pcolormesh(X,T,U,cmap='inferno')
#ax.set_xlabel("x")
#ax.set_ylabel("t")
#plt.tight_layout()
#fig.show()
