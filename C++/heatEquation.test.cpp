#include <algorithm>
#include <stdexcept>
#include <vector>

#include <gtest/gtest.h>

#include "heatEquation.hpp"

TEST(Simluation, Grid){
  
  sim::Grid grid{100, 1.0};
  
  EXPECT_DOUBLE_EQ(grid(0),0.0);
  EXPECT_DOUBLE_EQ(grid(grid.N()),grid.L());

  EXPECT_EQ(grid,grid);
  EXPECT_NE(grid,sim::Grid{});

}

TEST(Simluation, SimulationTime){
  
  sim::SimulationTime simTime{};
  double dt{0.01};

  EXPECT_DOUBLE_EQ(simTime.t(),0);
  EXPECT_DOUBLE_EQ(simTime.n(),0);
  simTime.advance_t_by(dt);
  EXPECT_DOUBLE_EQ(simTime.t(),dt);
  EXPECT_DOUBLE_EQ(simTime.n(),1);

  sim::SimulationTime iterTime{0.13,1.0};
  EXPECT_EQ(iterTime.T(), 1.0);
  EXPECT_EQ(iterTime.dt(), 0.13);
  iterTime.advance_t_by_dt();
  iterTime.advance_t_by_dt();
  EXPECT_EQ(iterTime.t(),2*iterTime.dt());
  EXPECT_EQ(iterTime.n(),2);
  EXPECT_EQ(iterTime.final_time_reached(), false);
  for(size_t i=2;i<10;i++)
    iterTime.advance_t_by_dt();
  EXPECT_GE(iterTime.t(),iterTime.T());
  EXPECT_EQ(iterTime.final_time_reached(), true);
  sim::SimulationTime iterate{0.03,1.0e12};
  for(;iterate.final_time_reached();iterate.advance_t_by_dt()){}
  EXPECT_EQ(iterate.t(),iterate.n()*iterate.dt());

}


TEST(Simulation, HeatDistribution){
  
  sim::Grid grid{100,1.0};
  sim::HeatDistribution heatDistribution{grid};

  EXPECT_NO_THROW(heatDistribution.at(0) = 0.0);
  EXPECT_NO_THROW(heatDistribution.at(grid.N()-1) = 0.0);
  EXPECT_EQ(heatDistribution.grid(),grid);
  EXPECT_EQ(heatDistribution.t(), 0);
  double dt{0.01};
  heatDistribution.advance_t_by(dt);
  EXPECT_DOUBLE_EQ(heatDistribution.t(), dt);

}

TEST(Simulation, FiniteDifferencePropagator){

  sim::Grid grid{100,1.0};
  sim::HeatDistribution start{grid};
  double dt{0.0};
  sim::HeatDistribution result = sim::propagate(start, dt);
  EXPECT_EQ(result.grid(),start.grid());
  EXPECT_EQ(result(0),0.0);
  EXPECT_EQ(result(grid.N()-1),0.0);
  start(1) = 1.0;
  result = sim::propagate(start, dt);
  EXPECT_EQ(result(1),start(1));
  start(start.grid().N()-2) = 1.0;
  result = sim::propagate(start, dt);
  EXPECT_EQ(result(result.grid().N()-2),start(start.grid().N()-2));
  dt=0.1;
  result = sim::propagate(start, dt);
  EXPECT_GT(result(2),0.0);
  EXPECT_EQ(result.t(),dt);
  result = sim::propagate(result, dt);
  EXPECT_EQ(result.t(),2*dt);

}

TEST(Simulation,SecondDerivative){

  EXPECT_EQ(sim::second_derivative(0,0,0,1),0);
  EXPECT_EQ(sim::second_derivative(1,4,9,1),2);

}
