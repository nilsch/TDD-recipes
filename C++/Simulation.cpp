#include <map>

#include "heatEquation.hpp"
#include "io.hpp"

int main(int argc, char** argv){
  
  sim::HeatEquationInitialize heatEquationInitialize{argc, argv};
  sim::SimulationTime simulationTime{sim::create_simulation_time_from_cmd_args()};
  std::map<int,sim::HeatDistribution> thermalDistribution{};
  thermalDistribution[0] = sim::create_thermal_distribution_from_file(
      heatEquationInitialize.value_of_flag("--sim-filename"));

  for(;not simulationTime.final_time_reached();simulationTime.advance_t_by_dt()){
    thermalDistribution[simulationTime.n()+1] = sim::propagate(
        thermalDistribution[simulationTime.n()], simulationTime.dt());
    sim::save_thermal_distribution_to_file(thermalDistribution[simulationTime.n()+1],
                                           heatEquationInitialize.value_of_flag("--sim-filename"));
  }
  return EXIT_SUCCESS;
  
}
