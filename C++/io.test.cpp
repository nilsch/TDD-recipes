#include <string>
#include <type_traits>
#include <utility>

#include <gtest/gtest.h>

#include "heatEquation.hpp"
#include "io.hpp"


class SimInputCMD : public testing::Test{
public:
    SimInputCMD() = default;
    int argc{4};
    char *argv[4] ={(char *)("/Full/Path/Of/Executable"),
                     (char *)("--sim-end-time"),
                     (char *)("5.0"),
                     (char *)("--random-other-flag")};

};

TEST_F(SimInputCMD,ParseCMDToVectorOfStrings){
  std::vector<std::string> cmdArgs{sim::parse_cmd_to_vector_of_strings(argc,argv)};
  EXPECT_STREQ(cmdArgs[0].c_str(),argv[0]);
  EXPECT_STREQ(cmdArgs[1].c_str(),argv[1]);
  EXPECT_EQ(cmdArgs.size(),4);
  EXPECT_THROW(sim::parse_cmd_to_vector_of_strings(0,argv),std::runtime_error);
}

TEST_F(SimInputCMD,CMDArgumentsDefault){
  argc = 1;
  sim::HeatEquationInitialize cmdArguments{argc, argv};
  EXPECT_STREQ(cmdArguments.value_of_flag("--sim-time-step").c_str(),"0.05");
  EXPECT_STREQ(cmdArguments.value_of_flag("--sim-end-time").c_str(),"10");
  EXPECT_THROW(cmdArguments.value_of_flag("--not-a-sim-flag"),std::runtime_error);
}

TEST_F(SimInputCMD,CMDArgumentsParsing){
  { sim::HeatEquationInitialize cmdArgumentsScope1{argc, argv}; }
  sim::HeatEquationInitialize cmdArgumentsScope2{};
  EXPECT_STREQ(cmdArgumentsScope2.value_of_flag("--sim-time-step").c_str(),"0.05");
  EXPECT_STREQ(cmdArgumentsScope2.value_of_flag("--sim-end-time").c_str(),argv[2]);
}

TEST_F(SimInputCMD,CMDArgumentsExceptions){
  sim::HeatEquationInitialize(argc,argv);
  EXPECT_THROW(sim::HeatEquationInitialize(argc,argv),std::runtime_error);
}

TEST_F(SimInputCMD,CreateSimulationTimeFromCMDArgs){
  EXPECT_THROW(sim::SimulationTime{sim::create_simulation_time_from_cmd_args()},std::runtime_error);
  sim::HeatEquationInitialize{argc,argv};
  sim::SimulationTime simTime{sim::create_simulation_time_from_cmd_args()};
  EXPECT_DOUBLE_EQ(simTime.dt(),0.05);
  EXPECT_DOUBLE_EQ(simTime.T(),5.0);

}

TEST(SimIO,ReadGridConfigFromInitFile){
  EXPECT_THROW(sim::create_grid_from_config_string("wrong"),std::runtime_error);
  EXPECT_THROW(sim::create_grid_from_config_string("L_max=3..1,N=10"),std::runtime_error);
  EXPECT_THROW(sim::create_grid_from_config_string("L_max=3.1,N=10."),std::runtime_error);
  sim::Grid grid{};
  EXPECT_NO_THROW(grid = sim::create_grid_from_config_string("L_max = 3.14 , N = 10 "));
  EXPECT_NO_THROW(grid = sim::create_grid_from_config_string("L_max = 3.14 , N = 10 (Grid Configuration)"));
  EXPECT_EQ(grid.N(),11);
  EXPECT_DOUBLE_EQ(grid(0),0.0);
}

TEST(SimIO,ReadThermalDistributionFromInitFile){
  EXPECT_THROW(sim::create_thermal_distribution_from_file("nonExistingFile.dat"),
               std::runtime_error);
  sim::HeatDistribution thermalDistribution(sim::create_thermal_distribution_from_file("thermalDistribution.test.dat"));
  //EXPECT_DOUBLE_EQ(thermalDistribution(0),0.0);
  //EXPECT_NEAR(thermalDistribution(1),sin(thermalDistribution.grid().dx()),1e-8);
  //sim::save_thermal_distribution_to_file(thermalDistribution,"thermalDistribution.test.dat");
}