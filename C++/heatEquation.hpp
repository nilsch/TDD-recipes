#pragma once

#include <cstddef>
#include <cmath>
#include <vector>

namespace sim {

/**
* Grid discretization of the heat equation
*/
class Grid{
  public:
  Grid() = default;
  /**
  * @brief Create a grid object
  *
  * @param N Cells within the grid
  * @param L Length of the gird
  */
  Grid(size_t N, double L) 
    : m_N{N+1}
  {
    m_dx = L/static_cast<double>(N);
    m_L = L + m_dx;
  };

  double operator()(size_t ix){ return m_dx*ix; };
  
  size_t N(){return m_N;}
  double L(){return m_L;}
  double dx(){return m_dx;}
  private:
  size_t m_N;
  double m_L;
  double m_dx;
};

class SimulationTime{
  public:
  SimulationTime() = default;
  SimulationTime(double dt, double T)
    : m_dt{dt}
    , m_T{T}
  {}

  void advance_t_by(double dt) {
      m_t += dt;
      m_n++;
  };
  void advance_t_by_dt(){
    m_t += dt();
    m_n++;
  }
  bool final_time_reached(){
    if(m_t<m_T) {
      return false;
    } else {
      return true;
    }
  }
  double dt() const {return m_dt; };
  double T()  const { return m_T; };
  double t()  const { return m_t; };
  size_t n()  const { return m_n; };

  private:
  double m_dt{};
  double m_T{};
  double m_n{};
  double m_t{};

};


bool operator==(Grid a, Grid b);
bool operator!=(Grid a, Grid b);


class HeatDistribution{

  public:
  HeatDistribution() = default;
  HeatDistribution(Grid grid)
    : m_grid{grid}
    , m_data{std::vector<double>(grid.N())}
     {};

  double& at(size_t i){ return m_data.at(i); }
  double& operator()(size_t i) { return m_data[i]; };
  Grid grid() const {return m_grid;};

  double t() const {return m_simTime.t();};
  void advance_t_by(double dt){m_simTime.advance_t_by(dt);};

  private:
  friend void initialize_heat_distribution(HeatDistribution &heatDistribution, double value);
  std::vector<double> m_data{};
  Grid m_grid{};
  SimulationTime m_simTime{};

};

/**
* @brief Finite differente second derivative
* 
* Returns the centered finite difference second derivative of order \f$ O(h^2) \f$
*
* @f[
* \dfrac{\mathrm{d}x}{\mathrm{d}t} \approx \dfrac{x_1 - 2x_2 + x_3}{h^2} + O(h^2)
* @f]
* 
* @param x1 left point in stencil
* @param x2 center point in stencil
* @param x3 right point in stencil
* @param dx grid spacing
*
*/
double second_derivative(double const x1, double const x2, double const x3, double const dx);
HeatDistribution propagate(HeatDistribution const start, double const dt);

}
