#include "heatEquation.hpp"

namespace sim{

bool operator==(Grid a, Grid b){
  double Lerr{};
  if(a.L()!=0.0) Lerr = abs((a.L() - b.L())/a.L());
  if(a.N() == b.N() and Lerr< 1e-15){
    return true;
  } else {
    return false;
  }
}

bool operator!=(Grid a, Grid b){
  return not (a==b);
}

double second_derivative(double const x1, double const x2, double const x3, double const dx){
  return (x1-2*x2+x3)/dx/dx;
}

HeatDistribution propagate(HeatDistribution start, double dt){
  HeatDistribution result{start.grid()};
  for(size_t i=1;i<start.grid().N()-1;i++)
    result(i) = second_derivative(start(i-1),start(i),start(i+1),start.grid().dx())*dt+start(i);
  result.advance_t_by(start.t()+dt);
  return result;
}

}