#pragma once

#include <algorithm>
#include <exception>
#include <string>
#include <utility>

#include "heatEquation.hpp"

namespace sim {
SimulationTime create_simulation_time_from_cmd_args();

std::vector<std::string> parse_cmd_to_vector_of_strings(int argc,char** argv);
bool is_cmd_argument_of_sim(std::string);
class HeatEquationInitialize {
  public:
  HeatEquationInitialize() = default;
  HeatEquationInitialize(int argc,char** argv);

  std::string value_of_flag(std::string flagName);
  private:

  void replace_default_values_by_cmd_values(int argc, char** argv);
  friend SimulationTime create_simulation_time_from_cmd_args();
  static bool m_isInitialized;
  static std::vector<std::pair<std::string,std::string>> m_cmdArgs;
};

Grid create_grid_from_config_string(std::string filename);
HeatDistribution create_thermal_distribution_from_file(std::string filename);
void save_thermal_distribution_to_file(HeatDistribution thermalDistribution, std::string filename);

}