#include <algorithm>
#include <fstream>
#include <iostream>
#include <regex>
#include <utility>
#include <vector>

#include "heatEquation.hpp"
#include "io.hpp"

namespace sim{

const std::vector<std::pair<std::string,std::string>> simCMDFlagsDefaultValue{
    std::pair<std::string,std::string>{"--sim-end-time","10"},
    std::pair<std::string,std::string>{"--sim-time-step","0.05"},
    std::pair<std::string,std::string>{"--sim-filename","thermalDistribution.dat"}
};

std::vector<std::string> parse_cmd_to_vector_of_strings(int argc,char** argv){
  if(argc <= 0) throw std::runtime_error("No CMDArguments have been given to pass argc<=0");
  std::vector<std::string> cmdArgs{};
  for(int i=0; i<argc;i++) cmdArgs.push_back(argv[i]);
  return cmdArgs;
}

bool HeatEquationInitialize::m_isInitialized{false};
std::vector<std::pair<std::string,std::string>> HeatEquationInitialize::m_cmdArgs{simCMDFlagsDefaultValue};

HeatEquationInitialize::HeatEquationInitialize(int argc,char** argv) {
  if(m_isInitialized) {
    throw std::runtime_error(
        "CMDArguments was already initialized with cmd arguments");
  }
  m_isInitialized = true;
  this->replace_default_values_by_cmd_values(argc, argv);
}

std::string HeatEquationInitialize::value_of_flag(std::string flagName) {
  for (auto argPair : m_cmdArgs)
    if(argPair.first == flagName) return argPair.second;
  throw std::runtime_error("The flag "+flagName+"is not a flag of sim");
}

void HeatEquationInitialize::replace_default_values_by_cmd_values(int argc, char **argv) {
  std::vector<std::string> cmdArgs{parse_cmd_to_vector_of_strings(argc,argv)};
  for(auto& argPair : m_cmdArgs){
    for(auto cmdArg=cmdArgs.begin();cmdArg!=cmdArgs.end();cmdArg++){
      if(*cmdArg == argPair.first) {
        cmdArg++;
        argPair.second = *cmdArg;
      }
    };
  }
}

SimulationTime create_simulation_time_from_cmd_args(){
  HeatEquationInitialize initialize{};
  if(not initialize.m_isInitialized)
    throw std::runtime_error("HeatEquationInitialize has not been called so far.\n"
                           "Initialize HeatEquationInitialize to use "
                           "create_simulation_time_from_cmd_args()");
  double dt = std::stod(initialize.value_of_flag("--sim-time-step"));
  double T = std::stod(initialize.value_of_flag("--sim-end-time"));
  return SimulationTime{dt,T};
}

std::string doubleRegexString(R"(([+-]?(?:\d+\.?|\d*\.\d+))(?:[Ee][+-]?\d+)?)");
Grid create_grid_from_config_string(std::string configString){
  configString = std::regex_replace(configString, std::regex{R"(\s+)"},"");
  std::regex configRegex{R"(^L_max=(\d*\.\d+),N=([1-9]\d*)(?:\(\w*\))?$)"};
  if(not std::regex_match(configString,configRegex)) throw std::runtime_error("Wrong RegEx for configuration string.\n"
                             "Required format: 'L_max=<double>,N=<int>' with L_max > 0.");

  std::smatch match_N{};
  std::regex_search(configString,match_N,std::regex{"N="});
  size_t N = std::stoi(match_N.suffix().str());
  std::smatch match_L_max{};
  std::regex_search(configString,match_L_max,std::regex{R"((\d*\.\d+))"});
  double LMax = std::stod(match_L_max[0]);

  return Grid{N,LMax};
}

HeatDistribution create_thermal_distribution_from_file(std::string filename){
  std::ifstream inputStream{filename};
  if(not inputStream.is_open()) throw std::runtime_error("File '"+filename+"' could not be opened!");
  std::string configString{};
  std::getline(inputStream,configString);
  HeatDistribution thermalDistribution{create_grid_from_config_string(configString)};
  std::string nonUsedString{};
  std::getline(inputStream,nonUsedString);
  std::string initHeatDistData;
  std::getline(inputStream,initHeatDistData);
  initHeatDistData = std::regex_replace(initHeatDistData, std::regex{R"(\s+)"},"");
  std::smatch heatDistMatch{};
  std::regex_search(initHeatDistData,heatDistMatch,std::regex{doubleRegexString+","});
  initHeatDistData = heatDistMatch.suffix();
  for(size_t i=0; i<thermalDistribution.grid().N(); i++) {
    std::regex_search(initHeatDistData,heatDistMatch,std::regex{doubleRegexString+",?"});
    std::string gridValue {std::regex_replace(heatDistMatch.str(), std::regex{","},"")};
    thermalDistribution.at(i) = std::stod(gridValue);
    initHeatDistData = heatDistMatch.suffix();
  }

  return thermalDistribution;
};

void save_thermal_distribution_to_file(HeatDistribution thermalDistribution, std::string filename){
  std::ofstream outputStream{filename,std::ios::app};
  outputStream << thermalDistribution.t();
  for(size_t i=0;i<thermalDistribution.grid().N();i++) {
    outputStream << " , " << thermalDistribution(i);
  }
  outputStream << "\n";

}

};